
const checkWinner = (playerOneScore, playerTwoScore) => {
    console.log(playerOneScore, playerTwoScore)
    if (playerOneScore > playerTwoScore) {
        document.querySelector("h1").innerHTML = "🚩 Play 1 Wins!";
    }
    else if (playerTwoScore > playerOneScore) {
        document.querySelector("h1").innerHTML = "Player 2 Wins! 🚩";
    }
    else {
        document.querySelector("h1").innerHTML = "Draw!";
    }
}

export default checkWinner;